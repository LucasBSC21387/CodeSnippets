#ifdef OACC
module popo

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Placeholder module for MPI-->GPU association subroutine.         !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   contains

      subroutine setDevice(nprocs,myrank,numdev,ierr)

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ! Needs verification: inn principle, this should bind an MPI rank  !
         ! to a single GPU, keeping in mind the ID of the host node.        !
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
         use iso_c_binding
         use openacc
         use mpi
      
         implicit none
      
         interface
            function gethostid() BIND(C)
               use iso_c_binding
               integer (C_INT) :: gethostid
            end function gethostid
         end interface
      
         integer(4), intent(in)    :: nprocs, myrank
         integer(4), intent(out)   :: numdev
         integer(4), intent(inout) :: ierr
         integer(4)                :: hostids(nprocs), localprocs(nprocs)
         integer(4)                :: hostid, mydev, i, numlocal
      
         ! Get hostids from all nodes involved
         hostid = gethostid()
         call MPI_Allgather(hostid,1,MPI_INTEGER,hostids,1,MPI_INTEGER, &
            MPI_COMM_WORLD,ierr)
      
         ! Determine which procs are in this node
         numlocal = 0
         localprocs = 0
         do i = 1,nprocs
            if (hostid .eq. hostids(i)) then
               localprocs(i) = numlocal
               numlocal = numlocal+1
            end if
         end do
      
         ! Get number of devices on the node
         numdev = acc_get_num_devices(acc_device_nvidia)
         if (numdev .lt. numlocal) then
            mydev = mod(localprocs(myrank+1),numdev)
         else
            mydev = localprocs(myrank+1)
         end if
      
         call acc_set_device_num(mydev,acc_device_nvidia)
         call acc_init(acc_device_nvidia)

      end subroutine setDevice

   end module popo
#endif

program vecAdd

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Small code to exemplify how MPI works with openACC when using    !
   ! managed memory compilation options. Scatters a set of 2 arrays   !
   ! across all requested MPIs, which then add their partials         !
   ! offloaded to their associated GPUs. End result obtained with a   !
   ! MPI_Gather on rank 0.                                            !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   use mpi
#ifdef OACC
   use openacc
   use popo
#endif

   implicit none

   integer(4)            :: ierr, checkOK
   integer(4)            :: nprocs, pid, npart
   integer(4)            :: numdev
   integer(4)            :: i, ipart, iter
   integer(4), parameter :: n = 512**3   ! Array size
   integer(4), parameter :: niter = 2000 ! Kernel repetitions
   real(8), allocatable  :: a(:), b(:), c(:)
   real(8), allocatable  :: apart(:), bpart(:), cpart(:)

   ierr = 0

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Initialize MPI environment                                       !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   call MPI_INIT(ierr)
   call MPI_COMM_SIZE(MPI_COMM_WORLD,nprocs,ierr)
   call MPI_COMM_RANK(MPI_COMM_WORLD,pid,ierr)

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Subroutine to associate an MPI rank to a GPU across multiplle    !
   ! nodes.                                                           !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef OACC
   call setDevice(nprocs,pid,numdev,ierr)
#endif

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Master creates global arrays, inits a and b                      !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if (pid == 0) then
      allocate(a(n))
      allocate(b(n))
      allocate(c(n))
      !$acc parallel loop
      do i = 1,n
         a(i) = 0.0d0
         b(i) = dble(i)
      end do
      !$acc end parallel loop
   end if

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Balanced array partition between all ranks including master      !
   ! ensure division result is an integer                             !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   npart = n/nprocs
   if (pid == 0) then
      write(*,*) "--| ENSURE ARRAY CAN BE EVENLY PARTITIONED !"
   end if

   allocate(apart(npart))
   allocate(bpart(npart))
   allocate(cpart(npart))

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Master scatters global arrays into rank partitions               !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !
   ! MPI_Scatter:
   ! A root process will distribute the workload across all MPI ranks.
   ! Each rank will receive a different chunk of the global data.
   ! Notice that c is not scattered as it is computed by the ranks!
   ! It takes the following arguments:
   !
   ! Send data (global array)
   ! Send data size (partition size)
   ! Send MPI data type
   ! Receive data (array chunk)
   ! Receive size (partition size)
   ! Receive MPI data type
   ! Sender (root rank)
   ! Receiver (all ranks)
   ! Error checker (optional)
   !
   call MPI_Scatter(a,npart,MPI_DOUBLE,apart,npart,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
   call MPI_Scatter(b,npart,MPI_DOUBLE,bpart,npart,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! All ranks call the vector add kernel using their partitions      !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   do iter = 1,niter
      !do ipart = 1,npart
      !   cpart(ipart) = 0.0d0
      !end do
      call kernel_vAdd(npart,apart,bpart,cpart)
   end do

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Master gathers results from all ranks into global array          !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !
   ! MPI_Gather:
   ! It is the inverse of scatter: the root processor will get the
   ! chunks from all ranks and map them into the global array.
   ! Arguments are similar to scatter, except that the send data
   ! is indeed the data sent by the multiple ranks to the master.
   !
   call MPI_Gather(cpart,npart,MPI_DOUBLE,c,npart,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Master gathers results from all ranks into global array          !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if (pid == 0) then
      write(*,*) "c(1) = ",c(1)
      write(*,*) "c(n/2) = ",c(n/2)
      write(*,*) "c(n) = ",c(n)
      write(*,*) "*** Program finished! ***"
   end if


   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Finalize MPI environment                                         !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   call MPI_FINALIZE(ierr)

end program vecAdd

pure subroutine kernel_vAdd(n,a,b,c)

   implicit none

   integer(4), intent(in)  :: n
   real(8),    intent(in)  :: a(n), b(n)
   real(8),    intent(out) :: c(n)
   integer(4)              :: i

   !$acc parallel loop
   do i = 1,n
      c(i) = a(i)+b(i)
   end do
   !$acc end parallel loop

end subroutine kernel_vAdd
