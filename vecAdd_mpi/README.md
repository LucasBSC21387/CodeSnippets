# Description

Simple vector addition example to demonstrate how to communicate with multiple GPUs using point-to-point MPI. In
essence, global vectors a and b are created by the master, then scattered by it to all other ranks. Each rank then calls
the vecAdd kernel over its own partition. The kernel does c_i = a_i+b_i utilizing OpenACC to accelerate the loop. Memory
management (data transfer from host to device) is managed automatically by using "-gpu=managed" compilaation option.

# Pre-requisites

- NVIDIA-HPC-SDK (any version);
- CUDA-aware MPI (provided on the nvhpc package);

# Building


- With OpenACC:

```
mpif90 -fast -DOACC -gpu=ccXY,managed,lineinfo -cuda -acc -o [ExecName] vecAdd_mpi.f90
```

- MPI only:

```
mpif90 -fast -o [ExecName] vecAdd_mpi.f90
```

# Usage

Wheter the code was built with OpenACC or not, the global array must be partitioned evenly between all ranks. Either
adjust the vector size on the code, or select an appropriate number of CPUs for the task. In case the code is using both
MPI and OpenACC, check wheter there are enough GPUs available. The code will accept multiple CPUs per GPU, but
it is not ideal. In any case, the executable should be run with:

```
mpirun -np NPROCS ./[ExecName]
```
