/*
    HASH TABLE LIBRARY

    C implementation of a hash table algorithm to quickly index
    a table using a string key.

    ref: https://www.digitalocean.com/community/tutorials/hash-table-in-c-plus-plus

    Arnau Miro (2022)
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hash_table.h"


/*
    Hash functions

    ref: http://www.cse.yorku.ca/~oz/hash.html
*/
#define HTFUN(name) hash_function_ ## name
#define HASH_FUNCTION(name,str,sz) HTFUN(name)(str,sz)

unsigned long hash_function_djb2(char *str, unsigned long sz) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    return hash % sz;
}

unsigned long hash_function_sdbm(char *str, unsigned long sz) {
    unsigned long hash = 0;
    int c;
    while ((c = *str++))
        hash = c + (hash << 6) + (hash << 16) - hash;
    return hash % sz;
}

unsigned long hash_function_loselose(char *str, unsigned long sz) {
    unsigned int hash = 0;
    int c;
    while ((c = *str++))
        hash += c;
    return hash % sz;
}


/*
    Array methods
*/
void alloc_array(array* a, int sz, int t) {
    if (a->t == A_EMPTY)
        switch (t) {
            case A_FLOAT:  a->sz=sz; a->t=t; a->v.f=(float*)malloc(a->sz*sizeof(float)); break;
            case A_DOUBLE: a->sz=sz; a->t=t; a->v.d=(double*)malloc(a->sz*sizeof(double)); break;
            default: break;
        }
}
void set_array_float(array* a, float* v, int sz)   { if (a->t == A_FLOAT  && a->sz == sz) memcpy(a->v.f,v,a->sz*sizeof(float)); }
void set_array_double(array* a, double* v, int sz) { if (a->t == A_DOUBLE && a->sz == sz) memcpy(a->v.d,v,a->sz*sizeof(double)); }
array create_array_float(float* v, int sz)         { array a; a.t=A_EMPTY; alloc_array(&a,sz,A_FLOAT);  set_array_float(&a,v,sz);  return a; }
array create_array_double(double* v, int sz)       { array a; a.t=A_EMPTY; alloc_array(&a,sz,A_DOUBLE); set_array_double(&a,v,sz); return a; }
array view_array_float(float* v, int sz)           { array a; a.sz=sz; a.v.f = v; a.t = A_FLOAT;  return a; }
array view_array_double(double* v, int sz)         { array a; a.sz=sz; a.v.d = v; a.t = A_DOUBLE; return a; }
float*  get_array_float(array* a)                  { return a->v.f; }
double* get_array_double(array* a)                 { return a->v.d; }
float  get_array_float_value(array* a, int i)      { return a->v.f[i]; }
double get_array_double_value(array* a, int i)     { return a->v.d[i]; }
void free_array(array* a)                     { 
    switch (a->t) {
        case A_FLOAT:  free(a->v.f); a->sz=0; a->t=A_EMPTY; break;
        case A_DOUBLE: free(a->v.d); a->sz=0; a->t=A_EMPTY; break;
        default: break;
    }
}
void deep_copy_array(array* a1, array* a2) { 
    // a1 is deallocated and a2 is copied into a1
    free_array(a1);
    alloc_array(a1,a2->sz,a2->t);
    switch (a1->t) {
        case A_FLOAT:  memcpy(a1->v.f,a2->v.f,a2->sz*sizeof(float)); break;
        case A_DOUBLE: memcpy(a1->v.d,a2->v.d,a2->sz*sizeof(double)); break;
        default: break;
    }
}
void print_array(array* a) { 
    switch (a->t) {
        case A_FLOAT:  printf("%f - %f, sz=%d",a->v.f[0],a->v.f[a->sz-1],a->sz); break;
        case A_DOUBLE: printf("%f - %f, sz=%d",a->v.d[0],a->v.d[a->sz-1],a->sz); break;
        default:       printf("empty, sz=%d",a->sz); break;
    }
}


/*
    Generic type methods
*/
// Set methods
void set_T_int   (T* x, int    v) {x->value.i = v; x->itype=TYPE_INT;   }
void set_T_long  (T* x, long   v) {x->value.l = v; x->itype=TYPE_LONG;  }
void set_T_float (T* x, float  v) {x->value.f = v; x->itype=TYPE_FLOAT; }
void set_T_double(T* x, double v) {x->value.d = v; x->itype=TYPE_DOUBLE;}
void set_T_string(T* x, char*  v) {x->value.s = v; x->itype=TYPE_STRING;}
void set_T_array (T* x, array* v) {x->value.a = v; x->itype=TYPE_ARRAY; }
// Get methods
int     get_T_int   (T* x) {return x->value.i;}
long    get_T_long  (T* x) {return x->value.l;}
float   get_T_float (T* x) {return x->value.f;}
double  get_T_double(T* x) {return x->value.d;}
char*   get_T_string(T* x) {return x->value.s;}
array*  get_T_array (T* x) {return x->value.a;}
// Copy method
void copy_T(T* x1, T* x2) { // x1 = x2
    switch(x2->itype) {
        case TYPE_INT:    set_T_int(   x1,get_T_int(   x2)); break;
        case TYPE_LONG:   set_T_long(  x1,get_T_long(  x2)); break;
        case TYPE_FLOAT:  set_T_float( x1,get_T_float( x2)); break;
        case TYPE_DOUBLE: set_T_double(x1,get_T_double(x2)); break;
        case TYPE_STRING: set_T_string(x1,get_T_string(x2)); break;
        case TYPE_ARRAY:  set_T_array (x1,get_T_array(x2));  break;
    }
}
// Printing format
char* format_T(T* x) {
    switch(x->itype) {
        case TYPE_INT:    return "%d";
        case TYPE_LONG:   return "%ld";
        case TYPE_FLOAT:  return "%f";
        case TYPE_DOUBLE: return "%lf";
        case TYPE_STRING: return "%s";
        case TYPE_ARRAY:  return "";
    }
    return "";   
}
// Printing type
char* type_T(T* x) {
    switch(x->itype) {
        case TYPE_INT:    return "int";
        case TYPE_LONG:   return "long";
        case TYPE_FLOAT:  return "float";
        case TYPE_DOUBLE: return "double";
        case TYPE_STRING: return "string";
        case TYPE_ARRAY:  return "array";
    }
    return "none";    
}
// Print a value as a string
void print_T(T* x) {
    switch(x->itype) {
        case TYPE_INT:    printf("%d", x->value.i); break;
        case TYPE_LONG:   printf("%ld",x->value.l); break;
        case TYPE_FLOAT:  printf("%f", x->value.f); break;
        case TYPE_DOUBLE: printf("%lf",x->value.d); break;
        case TYPE_STRING: printf("%s", x->value.s); break;
        case TYPE_ARRAY:  print_array(x->value.a);  break;
    }
}


/* 
    Hash table item functions
*/
ht_item* create_item(char* key, T value) {
    // Creates a pointer to a new hash table item
    ht_item* item = (ht_item*)malloc(sizeof(ht_item));
    // Copy key
    item->key = (char*)malloc(strlen(key)+1);
    strcpy(item->key, key);
    // Copy value
    copy_T(&item->value,&value); // tem->value = value
    return item;
}

void free_item(ht_item* item) {
    // Frees an item
    free(item->key);
    free(item);
}


/*
    Liked list item functions
*/
struct _linkedList {
    ht_item*    item;
    linkedList* next;   
};

linkedList* allocate_linkedList () {
    // Allocates memory for a linkedList pointer
    linkedList* list = (linkedList*)malloc(1*sizeof(linkedList));
    return list;
}

linkedList* insert_linkedList(linkedList* list, ht_item* item) {
    // Inserts the item onto the Linked List
    if (!list) {
        linkedList* head = allocate_linkedList();
        head->item = item;
        head->next = NULL;
        list       = head;
        return list;
    } else if (list->next == NULL) {
        linkedList* node = allocate_linkedList();
        node->item = item;
        node->next = NULL;
        list->next = node;
        return list;
    }

    linkedList* temp = list;
    while (temp->next->next) {
        temp = temp->next;
    }
    
    linkedList* node = allocate_linkedList();
    node->item = item;
    node->next = NULL;
    temp->next = node;
    
    return list;
}

ht_item* pop_linkedList(linkedList* list) {
    // Removes the head from the linked list
    // and returns the item of the popped element
    if (!list) return NULL;
    if (!list->next) return NULL;
    linkedList* node = list->next;
    linkedList* temp = list;
    temp->next = NULL;
    list = node;
    ht_item* it = NULL;
    memcpy(temp->item, it, sizeof(ht_item));
    free_item(temp->item);
    free(temp);
    return it;
}

void free_linkedList(linkedList* list) {
    linkedList* temp = list;
    while (list != NULL) {
        temp = list;
        list = list->next;
        free_item(temp->item);
        free(temp);
    }
}


/*
    Hash table functions
*/
hash_table* create_table(char *name);
void free_table(hash_table* table);
void resize_table(hash_table* table);
void ht_insert_item(hash_table* table, char* key, ht_item* item);
ht_item* ht_search_item(hash_table* table, char* key, unsigned long sz);
ht_item* ht_pop_item(hash_table* table, char* key, unsigned long sz);

hash_table* create_table(char *name) {
    // Creates a new hash table
    hash_table* table = (hash_table*)malloc(sizeof(hash_table));
    table->size  = HT_SIZE;
    table->count = 0;
    // Set up table name
    table->name  = (char*)malloc(strlen(name)+1);
    strcpy(table->name,name);
    // Set up items and overflow buckets
    table->items           = (ht_item**)malloc(table->size*sizeof(ht_item*));
    table->overflowBuckets = (linkedList**)malloc(table->size*sizeof(linkedList*));
    for (unsigned long i=0; i<table->size; ++i) {
        table->items[i]           = NULL;
        table->overflowBuckets[i] = NULL;
    }
    return table;
}

void free_table(hash_table* table) {
    // Frees the table
    for (unsigned long i=0; i<table->size; i++) {
        ht_item* item = table->items[i];
        if (item != NULL) free_item(item);
        free_linkedList(table->overflowBuckets[i]);
    }
    free(table->name);
    free(table->items);
    free(table->overflowBuckets);
    free(table);
}


void resize_update_table(hash_table* table, char *key, unsigned long old_size) {
    // Obtain the items
    ht_item* old_item = ht_pop_item(table,key,old_size); // Pop it out of the list
    ht_item* new_item = ht_search_item(table,key,table->size);
    // See if the position we are trying to move is empty
    // if so we do not need to do anything
    if (old_item == NULL) return;
    // See if the position we are trying to occupy is full
    // if so then empty it first recursively
    if (new_item != NULL) resize_update_table(table,new_item->key,old_size);
    // If the destiny position is empty then insert the item
    ht_insert_item(table,key,old_item);
}

void resize_table(hash_table* table) {
    // Do not allocate past the maximum
    #ifdef HT_MAX_SIZE
    if (table->size+HT_SIZE == HT_MAX_SIZE) {
        fprintf(stderr,"Reached table maximum size!");
        exit(-1); // crash
    }
    #endif
    // Obtain all the keys of the table
    char **keys; keys = get_keys(table);
    // Increase the size of the table
    unsigned long old_size = table->size;
    table->size += HT_SIZE;
    table->items           = (ht_item**)realloc(table->items,table->size*sizeof(ht_item*));
    table->overflowBuckets = (linkedList**)realloc(table->overflowBuckets,table->size*sizeof(linkedList*));
    for (unsigned long i=old_size; i<table->size; ++i) {
        table->items[i]           = NULL;
        table->overflowBuckets[i] = NULL;
    }
    // Move the keys to the new indices
    for (int i=0; i<table->count; ++i) {
        resize_update_table(table,keys[i],old_size);
        free(keys[i]);
    }
    free(keys);
}

void handle_collision(hash_table* table, unsigned long index, ht_item* item) {
    linkedList* head = table->overflowBuckets[index];
    if (head == NULL) {
        // We need to create the list
        head                          = allocate_linkedList();
        head->item                    = item;
        head->next                    = NULL;
        table->overflowBuckets[index] = head;
    } else {
        if (strcmp(item->key, head->item->key) == 0) {
            free_item(head->item);
            // Scenario 1: update the value
            head->item = item;
        } else {
            // Scenario 2: collision
            // Insert to the list
            table->overflowBuckets[index] = insert_linkedList(head, item);
            return;
        }
    }
}

void ht_insert_item(hash_table* table, char *key, ht_item* item) {
    // Compute the index
    unsigned long index = HASH_FUNCTION(HT_HASH,key,table->size);
    // Get the item we have requested
    ht_item* current_item = table->items[index];
    // Do insert past the maximum
    #ifdef HT_MAX_SIZE
    if (table->count == HT_MAX_SIZE) return;
    #endif
    // Check if the current key exists
    if (current_item == NULL) {
        // Key does not exist
        // Check if table is full and if so extend it
        if (table->count == table->size)
            resize_table(table);
        // Insert directly
        table->items[index] = item; 
        table->count++;
        return;
    } else {
        if (strcmp(current_item->key, key) == 0) {
            free_item(table->items[index]);
            // Scaneario 1: update the value
            table->items[index] = item;
            return;
        } else {
            // Scenario 2: collision
            handle_collision(table,index,item);
            table->count++;
            return;
        }
    }
}

void add_item_T(hash_table* table, char* key, T value) {
    // Create the item
    ht_item* item = create_item(key, value);
    // Insert on the table
    ht_insert_item(table,key,item);
}

void add_item_int   (hash_table* table, char* key, int    value) { T v; set_T_int(&v,value);    add_item_T(table,key,v); }
void add_item_long  (hash_table* table, char* key, long   value) { T v; set_T_long(&v,value);   add_item_T(table,key,v); }
void add_item_float (hash_table* table, char* key, float  value) { T v; set_T_float(&v,value);  add_item_T(table,key,v); }
void add_item_double(hash_table* table, char* key, double value) { T v; set_T_double(&v,value); add_item_T(table,key,v); }
void add_item_string(hash_table* table, char* key, char*  value) { T v; set_T_string(&v,value); add_item_T(table,key,v); }
void add_item_array (hash_table* table, char* key, array* value) { T v; set_T_array(&v,value);  add_item_T(table,key,v); }

ht_item* ht_search_item(hash_table* table, char* key, unsigned long sz) {
    // Searches the key in the hashtable
    // and returns NULL if it doesn't exist
    unsigned long index = HASH_FUNCTION(HT_HASH,key,sz);
    ht_item* item    = table->items[index];
    linkedList* head = table->overflowBuckets[index];
    // Ensure that we move to items which are not NULL
    while (item != NULL) {
        if (strcmp(item->key, key) == 0)
            return item;
        if (head == NULL)
            return NULL;
        item = head->item;
        head = head->next;
    }
    return NULL;
}

void search_item_T(hash_table* table, char* key, T* v) {
    // Searches the key in the hashtable and returns HT_EMPTY if it doesn't exist
    ht_item* item = ht_search_item(table,key,table->size);
    if(item != NULL) copy_T(v,&item->value);
}

int    search_item_int   (hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_int(&v);    }
long   search_item_long  (hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_long(&v);   }
float  search_item_float (hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_float(&v);  }
double search_item_double(hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_double(&v); }
char*  search_item_string(hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_string(&v); }
array* search_item_array (hash_table* table, char* key) { T v; search_item_T(table,key,&v); return get_T_array(&v);  }

ht_item* ht_pop_item(hash_table* table, char* key, unsigned long sz) {
    // Deletes an item from the table and returns
    // the item
    unsigned long index = HASH_FUNCTION(HT_HASH,key,sz);
    ht_item* item    = table->items[index];
    linkedList* head = table->overflowBuckets[index];

    if (item == NULL) {
        // Does not exist. Return
        return item;
    } else {
        if (head == NULL && strcmp(item->key, key) == 0) {
            // No collision chain. Remove the item
            // and set table index to NULL
            table->items[index] = NULL;
            table->count--;
            return item;
        } else if (head != NULL) {
            // Collision Chain exists
            if (strcmp(item->key, key) == 0) {
                // Remove this item and set the head of the list
                // as the new item
                linkedList* node = head;
                head       = head->next;
                node->next = NULL;
                table->items[index] = create_item(node->item->key, node->item->value);
                free_linkedList(node);
                table->overflowBuckets[index] = head;
                table->count--;
                return item;
            }

            linkedList* curr = head;
            linkedList* prev = NULL;
            
            while (curr) {
                if (strcmp(curr->item->key, key) == 0) {
                    if (prev == NULL) {
                        // First element of the chain. Remove the chain
                        free_linkedList(head);
                        table->overflowBuckets[index] = NULL;
                        return item;
                    }
                    else {
                        // This is somewhere in the chain
                        prev->next = curr->next;
                        curr->next = NULL;
                        free_linkedList(curr);
                        table->overflowBuckets[index] = head;
                        return item;
                    }
                }
                curr = curr->next;
                prev = curr;
            }
        }
    }
}

void pop_item_T(hash_table* table, char* key, T* v) {
    // Searches the key in the hashtable and returns HT_EMPTY if it doesn't exist
    ht_item* item = ht_pop_item(table,key,table->size);
    if(item != NULL) {
        copy_T(v,&item->value);
        free_item(item);
    }
}

int    pop_item_int   (hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_int(&v);    }
long   pop_item_long  (hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_long(&v);   }
float  pop_item_float (hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_float(&v);  }
double pop_item_double(hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_double(&v); }
char*  pop_item_string(hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_string(&v); }
array* pop_item_array (hash_table* table, char* key) { T v; pop_item_T(table,key,&v); return get_T_array(&v);  }

char** get_keys(hash_table* table) {
    ht_item* item;
    linkedList *head;
    int icount = 0;
    // Recover all the keys of the table
    char** keys = (char**)malloc(table->count*sizeof(char*));
    // Loop over the stored items
    for (int i=0; i<table->size; ++i) {
        item = table->items[i];
        head = table->overflowBuckets[i];
        // If the item does not exist continue searching
        if (item==NULL) continue;
        // Item exists, copy the key
        keys[icount] = (char*)malloc(strlen(item->key));
        strcpy(keys[icount],item->key);
        ++icount;
        // Handle collisions
        while (head != NULL) {
            item = head->item;
            head = head->next;
            // Recover the item
            keys[icount] = (char*)malloc(strlen(item->key));
            strcpy(keys[icount],item->key);
            ++icount;
        } 
    }
    return keys;
}


/*
    Print functions
*/
void print_search(hash_table* table, char* key) {
    ht_item* item = ht_search_item(table,key,table->size);
    if (item == NULL)
        printf("Key:%s does not exist\n",key);
    else {
        printf("Key:%s, Value:",item->key);
        print_T(&item->value);
        printf(" (%s)\n",type_T(&item->value));
    }
}

void print_table(hash_table* table) {
    ht_item* item;
    linkedList *head;
    printf("Table <%s>:\n-------------------\n",table->name);
    // Loop over the stored items
    for (int i=0; i<table->size; ++i) {
        item = table->items[i];
        head = table->overflowBuckets[i];
        // If the item does not exist continue searching
        if (item==NULL) continue;
        // Recover the item
        printf("Index:%lu,\tKey:%s,\tValue:",i,item->key);
        print_T(&item->value);
        printf(" (%s)\n",type_T(&item->value));
        // Handle collisions
        while (head != NULL) {
            item = head->item;
            head = head->next;
            // Recover the item
            printf("Index:%lu,\tKey:%s,\tValue:",i,item->key);
            print_T(&item->value);
            printf(" (%s)\n",type_T(&item->value));
        }
    }
    printf("-------------------\n");
}
