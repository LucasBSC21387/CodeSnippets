/*
    HASH TABLE LIBRARY

    Main code example

    Arnau Miro (2022)
*/
#include <stdlib.h>
#include <stdio.h>

#include "hash_table.h"

int main(int argc,char **argv) {
    // Create tables
    hash_table* table = create_table("test table");

    // Add items to the tables
    add_item(table,"test_key1",123);    // int
    add_item(table,"test_key2",3.1415); // double
    add_item(table,"test_key3","test"); // string
    // array
    double vals[] = {1.,3.,6.};
    array arr1 = view_array(vals,3);    // soft copy
    add_item(table,"test_key4",&arr1);  // array
    array arr2 = create_array(vals,3);  // deep copy
    add_item(table,"test_key5",&arr2);  // array

    // Do some printing
    print_search(table,"test_key_missing");
    print_search(table,"test_key1");
    print_table(table);

    // Find all keys in the table
    char **keys;
    keys = get_keys(table);
    printf("Keys in table: [");
    for (int ikey=0; ikey<table->count; ++ikey){
        printf((ikey == table->count-1) ? "'%s'" : "'%s',",keys[ikey]);
        free(keys[ikey]);
    }
    printf("]\n");
    free(keys);

    // Now try recovering a key
    double value = search_item(table,"test_key2",value);
    printf("Found value is <%lf>\n",value);

    // Now try deleting a key
    char *str = pop_item(table,"test_key3",str);
    printf("Popped string is <%s>\n",str);
    print_table(table);

    // Free created table
    free_table(table);
    free_array(&arr2); // Must be freed since create_array allocates memory
}