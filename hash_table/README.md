# Description

An example of a hash table implementation in C. It is programmed in such a way that the defined macros set the type of the value stored by a string key. See the details on **Building** and **Usage**, as well as the example *main.c*.

# Pre-requisites

- C compiler;
- bash interpreter;

# Building

Compile the code using gcc:
```bash
gcc -o [ExecName] hash_table_int.c hash_table_float.c main.c
```

# Usage

To run the test code simply issue
```bash
./[ExecName]
```

## Code guidelines

Simply include the required header on the C code
```C
#include "hash_table.h"
```
Then, create the table as
```C
hash_table* table = create_table("[name]");
```
with the required type. A *name* is given on the table for identification purposes. Operations on the tables can be done through the following methods:
```C
// Create and delete
hash_table* table = create_table(name);
free_table(table);
// Operate
[type] value = search_item(table,key,value);
add_item(table,key,value);
[type] value = pop_item(table,key,value);
char** get_keys(table); // Memory must be deallocated manually
// Print
print_search(table,key);
print_table(table);
```

## Dealing with arrays

The hash table can store arrays through the array structure provided within the library. The following methods are available:
```C
array create_array([type]* v, int sz);             // Copies the memory in v to the array (deep copy)
array view_array([type]* v, int sz);               // Copies the pointer into the array (shallow copy)
[type]* get_array(array* a,[type]* t);             // Gets the pointer to the array
[type] get_array_value(array* a, int i, [type] t); // Gets the value of the array at i position
void free_array(array* a);                         // Free memory for deep copied arrays
```
Usually the user is expected to manage its own memory, therefore *view_array* is the preferred method of defining the array structure.