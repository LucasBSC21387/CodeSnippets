/*
    HASH TABLE LIBRARY

    C implementation of a hash table algorithm to quickly index
    a table using a string key.

    ref: https://www.digitalocean.com/community/tutorials/hash-table-in-c-plus-plus

    Arnau Miro (2022)
*/

// Configuration
#define HT_SIZE  128
//#define HT_MAX_SIZE 16777216
#define HT_HASH  djb2
/*
    Options for hash functions:
        > djb2
        > sdbm
        > loselose
*/

// Create array type item
typedef struct _array {
    enum {
        A_EMPTY,
        A_FLOAT,
        A_DOUBLE
    }t;
    union {
        float *f;
        double *d;
    }v;
    int       sz;
}array;
array create_array_float(float* v, int sz);
array create_array_double(double* v, int sz);
#define create_array(v,sz) _Generic((v), \
    float*:  create_array_float, \
    double*: create_array_double \
)(v,sz)
array view_array_float(float* v, int sz);
array view_array_double(double* v, int sz);
#define view_array(v,sz) _Generic((v), \
    float*:  view_array_float, \
    double*: view_array_double \
)(v,sz)
float*  get_array_float(array* a);
double* get_array_double(array* a);
#define get_array(a,t) _Generic((t), \
    float*:  get_array_float, \
    double*: get_array_double \
)(a)
float  get_array_float_value(array* a, int i);
double get_array_double_value(array* a, int i);
#define get_array_value(a,i,t) _Generic((t), \
    float:  get_array_float_value, \
    double: get_array_double_value \
)(a,i)
void free_array(array* a);

// Create generic type item
typedef struct _T {
    // Enumeration to control data type
    enum {
        TYPE_INT,
        TYPE_LONG,
        TYPE_FLOAT,
        TYPE_DOUBLE,
        TYPE_STRING,
        TYPE_ARRAY
    } itype;
    // Union to store the value of the variable
    union {
        int    i;
        long   l;
        float  f;
        double d;
        char*  s;
        array* a;
    } value;
} T;

// Create the hash table item
typedef struct _ht_item {
    char* key;
    T     value;
} ht_item;

// Create linked list item
typedef struct _linkedList linkedList;

// Create the hash table item
typedef struct _hash_table {
    ht_item**     items;
    linkedList**  overflowBuckets;
    char*         name;
    unsigned long size;
    unsigned long count;
} hash_table;
// Hash table creation
hash_table* create_table(char *name);
void free_table(hash_table* table);
// Add to hash table
void add_item_int   (hash_table* table, char* key, int    value);
void add_item_long  (hash_table* table, char* key, long   value);
void add_item_float (hash_table* table, char* key, float  value);
void add_item_double(hash_table* table, char* key, double value);
void add_item_string(hash_table* table, char* key, char*  value);
void add_item_array (hash_table* table, char* key, array* value);
#define add_item(table,key,value) _Generic((value), \
    int:    add_item_int, \
    long:   add_item_long, \
    float:  add_item_float, \
    double: add_item_double, \
    char*:  add_item_string, \
    array*: add_item_array \
)(table,key,value)
// Recover from hash table
int    search_item_int   (hash_table* table, char* key);
long   search_item_long  (hash_table* table, char* key);
float  search_item_float (hash_table* table, char* key);
double search_item_double(hash_table* table, char* key);
char*  search_item_string(hash_table* table, char* key);
array* search_item_array (hash_table* table, char* key);
#define search_item(table,key,type) _Generic((type), \
    int:    search_item_int, \
    long:   search_item_long, \
    float:  search_item_float, \
    double: search_item_double, \
    char*:  search_item_string, \
    array*: search_item_array \
)(table,key)
// Pop from the hash table
int    pop_item_int   (hash_table* table, char* key);
long   pop_item_long  (hash_table* table, char* key);
float  pop_item_float (hash_table* table, char* key);
double pop_item_double(hash_table* table, char* key);
char*  pop_item_string(hash_table* table, char* key);
array* pop_item_array (hash_table* table, char* key);
#define pop_item(table,key,type) _Generic((type), \
    int:    pop_item_int, \
    long:   pop_item_long, \
    float:  pop_item_float, \
    double: pop_item_double, \
    char*:  pop_item_string, \
    array*: pop_item_array \
)(table,key)
// Obtain all keys from the table
char** get_keys(hash_table* table);
// Printing
void print_search(hash_table* table, char* key);
void print_table(hash_table* table);