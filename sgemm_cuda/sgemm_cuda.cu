/*
	SGEMM IN CUDA

	The present snippet presents two algorithms for matrix matrix multiplication. 
	A simple scheme and a tiled scheme. Tiling is a common technique used to improve 
	data locality and minimize accesses to main memory. 

	The goals of this snippet are:
	- How to create and work with 2D grids.
	- How to use shared memory to store values that are read by many threads within a 
	  thread block. (Shared memory tiling)
	- How to use registers to store values that are used many times by the same thread. 
	  (Register tiling)
	- Why the size of thread blocks is important when using shared memory.
*/

#include <stdio.h>
#include <stdlib.h>

// Helpful macros
#define CHECK_CUDA_ERROR() do { \
  cudaError_t err = cudaGetLastError(); \
  if( cudaSuccess != err) { \
	fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
	 __FILE__, __LINE__, cudaGetErrorString( err) ); \
	exit(EXIT_FAILURE); \
  } \
} while(0)

// Returns x/y ceiled to the next (upper) integer when x doesn't evenly 
// divide by y (uses integer arithmetic trickery)
#define div_and_ceil(x,y) (((x) - 1)/(y) + 1)

// Macros for accessing flattened matrices
#define A(row,col) A[row + (col)*m]
#define B(row,col) B[(row)*n + (col)]
#define C(row,col) C[row + (col)*m]

// Parameters of tile sizes (only needed for the tiled implementation)
#define TILE_SZ_M 128
#define TILE_SZ_N 16
#define TILE_SZ_K (TILE_SZ_M/TILE_SZ_N) // keep this ratio to ease B_s loading (i.e. # of elements in B_s == # of threads in a thread block)

// CUDA Kernel implementations
#ifdef SGEMM_SIMPLE

__global__ void sgemm_naive( const float *A, const float *B, float* C, int m, int n, int k ) {
/*
	Simple sgemm kernel implementation.
	
	Note: A and C are stored in memory in column major order, and B is stored in row major.
	  m -> #rows of A, rows of C
	  n -> #cols of B, cols of C
	  k -> #rows of B, cols of A
*/
	int idx = threadIdx.x + (blockIdx.x * blockDim.x);
	int idy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((idx < m) && (idy < n)) {
		float p_val = 0.;
		for (int idk = 0; idk < k; ++idk) 
			p_val += A(idx,idk)*B(idk,idy);
		C(idx,idy) = p_val;
	}
}

#else

__global__ void sgemm_volkov( const float *A, const float *B, float* C, int m, int n, int k ) {
/*
	Optimized sgemm kernel implementation with shared memory and register tiling.
	
	Note: A and C are stored in memory in column major order, and B is stored in row major.
	  m -> #rows of A, rows of C
	  n -> #cols of B, cols of C
	  k -> #rows of B, cols of A
*/
	// Shared memory allocation to store a tile of B
	__shared__ float B_s [TILE_SZ_K][TILE_SZ_N];

	// Compute thread's global row index (for A and C)
	const unsigned int im  = blockIdx.x*blockDim.x + threadIdx.x;
	// Compute the global column index of the first column processed by the thread (for B and C)   
	const unsigned int in  = blockIdx.y*TILE_SZ_N;

	// Privatization of output variables. Each thread computes a row of a tile of C.
	float c_reg[TILE_SZ_N];

	// Initialize output values
	for(unsigned int ii = 0; ii<TILE_SZ_N; ++ii) c_reg[ii] = 0.;

	// Loop over the input tiles following the K dimension
	for(unsigned int tileIdx=0; tileIdx<div_and_ceil(k,TILE_SZ_K); ++tileIdx) {
		// Compute the coordinates of the element of B_s loaded by each thread 
		const unsigned int iB = threadIdx.x / TILE_SZ_N;
		const unsigned int jB = threadIdx.x % TILE_SZ_N;
		// Load the current tile of B into shared memory. Ensure all threads finished before proceeding.
		const unsigned int iiB = tileIdx*TILE_SZ_K + iB;
		const unsigned int jjB = in+jB;
		B_s[iB][jB] = (iiB<k && jjB<n) ? B(iiB,jjB) : 0.;
		__syncthreads();

		// Loop over the columns of A's tile and the rows of B's tile
		for(unsigned int idx=0; idx<TILE_SZ_K; ++idx) {
			// Load current element of A matrix into the register
			const unsigned int aIdx = tileIdx*TILE_SZ_K + idx;
			float a_reg = (im<m && aIdx<k) ? A(im,aIdx) : 0.;

			// Loop over the columns of B_s and update the output elements assigned to the thread
			for(int ii = 0; ii<TILE_SZ_N; ++ii) {
				c_reg[ii] += a_reg*B_s[idx][ii];
			}
		}
		// Ensure all threads finished before proceeding.
		__syncthreads();
	}
	
	// Store the result to C
	for(unsigned int ii = 0; ii<TILE_SZ_N; ++ii) {
		if(im < m && in+ii<n) {
			C(im,in+ii) = c_reg[ii];
		}
	}
}

#endif

// Main sgemm function 
void sgemm( const float *A, const float *B, float* C, int m, int n, int k ) {

#ifdef SGEMM_SIMPLE
	printf("Using the SIMPLE kernel implementation\n");

	const unsigned block_sz = 16;
	const unsigned grid_x   = div_and_ceil(m,block_sz);
	const unsigned grid_y   = div_and_ceil(n,block_sz);

	dim3 grid( grid_x, grid_y ), threads( block_sz, block_sz );

	sgemm_naive<<<grid, threads>>>( A, B, C, m, n, k);
#else
	printf("Using the TILED kernel implementation\n");

	const unsigned grid_x = div_and_ceil(m,TILE_SZ_M);
	const unsigned grid_y = div_and_ceil(n,TILE_SZ_N);

	dim3 grid( grid_x, grid_y ), threads( TILE_SZ_M, 1 );

	sgemm_volkov<<<grid, threads>>>( A, B, C, m, n, k);
#endif

	CHECK_CUDA_ERROR();
}

// Helper functions
float readNumber(FILE* fp) {
	int ii = 0;
	char c, buffer[256];

	c = fgetc(fp);
	while ( c != ' ' && c != EOF ) {
		buffer[ii++] = c; 
		c = fgetc(fp);
	}
	buffer[ii] = '\0';
		
	return atof(buffer);
}

int readColMajorMatrixFile(const char *fn, int *nr_row, int *nr_col, float **v) {

	printf("Opening file: %s for reading.\n",fn);

	FILE* fp = fopen(fn, "r");
	if (fp == NULL) return 0;

	// Read # of rows and cols
	*nr_row = (int)(readNumber(fp));
	*nr_col = (int)(readNumber(fp));
	const int sz = (*nr_row)*(*nr_col);

	// Allocate output memory
	*v = (float*)malloc(sz*sizeof(float));
	if (v == NULL) return 0;

	// Load the values
	for(unsigned int ii=0; ii<sz; ++ii)
		(*v)[ii] = readNumber(fp);

	// Close the file
	fclose(fp);
	return 1;
}

int writeColMajorMatrixFile(const char *fn, int nr_row, int nr_col, float *v) {

	printf("Opening file: %s for writing.\n",fn);

	FILE* fp = fopen(fn, "w");
	if (fp == NULL) return 0;

	// Write # of rows and cols
	fprintf(fp,"%d %d",nr_row,nr_col);

	// Write the values
	const int sz = (nr_row)*(nr_col);
	for(unsigned int ii=0; ii<sz; ++ii)
		fprintf(fp," %f",v[ii]);

	// Close the file
	fclose(fp);
	return 1;
}

// Main function
int main (int argc, char *argv[]) {

	int matArow, matAcol;
	int matBrow, matBcol;
	size_t A_sz, B_sz, C_sz;

	float *dA, *dB, *dC;
	float *matA, *matBT, *matC;

	// load A
	readColMajorMatrixFile("matrix1.txt", &matArow, &matAcol, &matA);

	// load B^T, note swapped dimensions
	readColMajorMatrixFile("matrix2t.txt", &matBcol, &matBrow, &matBT);

	A_sz = matArow*matAcol*sizeof(float);
	B_sz = matBrow*matBcol*sizeof(float);
	C_sz = matArow*matBcol*sizeof(float);

	// CUDA memory allocation
	matC = (float*)malloc(C_sz);
	cudaMalloc((void**)&dA, A_sz); CHECK_CUDA_ERROR();
	cudaMalloc((void**)&dB, B_sz); CHECK_CUDA_ERROR();
	cudaMalloc((void**)&dC, C_sz); CHECK_CUDA_ERROR();

	// Copy A and B^T into device memory
	cudaMemcpy(dA,  matA, A_sz, cudaMemcpyHostToDevice); CHECK_CUDA_ERROR();
	cudaMemcpy(dB, matBT, B_sz, cudaMemcpyHostToDevice); CHECK_CUDA_ERROR();

	// Call sgemm
	sgemm(dA, dB, dC, matArow, matBcol, matAcol);

	// Copy C back to host memory
	cudaMemcpy(matC, dC, C_sz, cudaMemcpyDeviceToHost); CHECK_CUDA_ERROR();

	// Write C to file
	writeColMajorMatrixFile("output.txt", matArow, matBcol, matC);

	// Deallocate memory
	cudaFree(dA); CHECK_CUDA_ERROR();
	cudaFree(dB); CHECK_CUDA_ERROR();
	cudaFree(dC); CHECK_CUDA_ERROR();

	free(matA); free(matBT); free(matC);
	return 0;
}