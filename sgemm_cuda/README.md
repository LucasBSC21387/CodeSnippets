# Description

The present snippet presents two algorithms for matrix matrix multiplication. A simple scheme and a tiled scheme. Tiling is a common technique used to improve data locality and minimize accesses to main memory. 

The goals of this snippet are:
- How to create and work with 2D grids.
- How to use shared memory to store values that are read by many threads within a thread block. (Shared memory tiling)
- How to use registers to store values that are used many times by the same thread. (Register tiling)
- Why the size of thread blocks is important when using shared memory.

# Background

The snippet expects A and C matrices to be in Column Major layout (1) and B to be in
Row Major layout (2). 
1. Column Major layout: elements in the same column are placed into contiguous memory locations.
2. Row Major layout: elements in the same row are placed into contiguous memory locations

## Simple scheme
Each element of the Matrix-Matrix multiplication result (matrix C) is computed with the dot product of a row of the A matrix and a column of the B matrix:

```c
for (unsigned i = 0; i < ROWS_C; ++i) { // Row
	for (unsigned j = 0; j < COLUMNS_C; ++j) { // Column
		float sum = 0.0f;
		// Traverse row 'i' of A and column 'j' of B
		// Remember that A is in Column Major and
		// B is in Row Major layout
		for (unsigned c = 0; c < COLUMNS_A; ++c) {
			sum += A[<linear idx for A>] * B[<linear idx for B>];
		}
		C[<linear idx for C>] = sum;
	}
}
```

Since the kernel receives the matrices as pointers, they cannot access them using the row and column indexes directly (e.g. A[row][col] // won’t work). The linear index should be computed, using the corresponding row and column indexes, to make the access into the “flattened” matrix. The formula used to compute the linear index depends on which representation is used to flatten the matrix.

## Volkov scheme

The tiled sgemm uses the Volkov scheme to implement a highly efficient matrix multiplication. The
scheme of the algorithm performed by one thread block to compute its output matrix tile is shown in the next figure (shared memory and register used for the tiling are not pictured). Note that in this case the thread block is smaller than matrix C's tile, and the whole tile is computed with a kernel that has three nested loops.

<img src="https://gitlab.com/LucasBSC21387/CodeSnippets/-/raw/main/sgemm_cuda/VolkovScheme.png"/>

The tile dimensions are defined by the TILE_SZ_x constants. Matrix C tile dimensions are TILE_SZ_M rows per TILE_SZ_N columns. TILE_SZ_K is the number of columns of A's tiles, and the number of rows of B's tiles. The expected sgemm implementations must be able to handle matices of any size, even if they don't evenly divide by the tile sizes.

# Pre-requisites

- CUDA libraries (any version);

# Building

Simply with the standard nvcc:
```
nvcc -o [ExecName] sgemm_cuda.cu
```

To obtain the simple sgemm scheme use:
```
nvcc -DSGEMM_SIMPLE -o [ExecName] sgemm_cuda.cu
```

# Usage

The executable should be run with:

```
./[ExecName]
```
