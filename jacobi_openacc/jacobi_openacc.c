/*
	2D JACOBI ITERATION USING OPENACC

	The most difficult part of accelerator programming begins before the first line of code 
	is written. If your program is not highly parallel, an accelerator or coprocesor won't be 
	much use. Understanding the code structure is crucial if you are going to identify 
	opportunities and successfully parallelize a piece of code. The first step in OpenACC 
	programming then is to characterize the application. 

	This includes:
	- Understanding the program structure and how data is passed through the call tree
	- Profiling the CPU-only version of the application and identifying 
	computationally-intense "hot spots"
		- Which loop nests dominate the runtime?
		- Are the loop nests suitable for an accelerator?
	- Ensuring that the algorithms you are considering for acceleration are safely parallel
	
	This may sound a little scary, but please note that as parallel programming methods go, 
	OpenACC is really friendly; because OpenACC directives are incremental, you can add one 
	or two directives at a time and see how things work. Also, the compiler provides a lot of 
	feedback that can help you during the acceleration process.

	In this example, an accelerated 2D-stencil Jacobi iteration is provided. Jacobi iteration 
	is a standard method for finding solutions to a system of linear equations.
*/

#include <math.h>
#include <string.h>
#include <stdio.h>

#include "timer.h"

#define NN 1024
#define NM 1024

float A[NN][NM], Anew[NN][NM];

int main(int argc, char** argv)
{
	const int n = NN;
	const int m = NM;
	const int iter_max = 1000;
	
	const double tol = 1.0e-6;
	double error     = 1.0;

	// Set matrix values
	memset(A, 0, n * m * sizeof(float));
	memset(Anew, 0, n * m * sizeof(float));

	for (int j = 0; j < n; j++) {
		A[j][0]    = 1.0;
		Anew[j][0] = 1.0;
	}
	
	printf("Jacobi relaxation calculation: %d x %d mesh\n", n, m);
	
	StartTimer();
	int iter = 0;
	
	// Since we need A and Anew on the device, here we specify that
	// A should be copied once to the device and Anew should be recovered
	// once the loop computations are done
	#pragma acc data copy(A), create(Anew)
	// Start of the Jacobi main loop
	while ( error > tol && iter < iter_max ) {
		// Here we tell OpenACC to generate accelerator kernels 
		// for the following loops
		#pragma acc kernels
		{
			error = 0.0;

			for( int j = 1; j < n-1; j++) {
				#pragma acc loop gang(8) vector(32)
				for( int i = 1; i < m-1; i++ ) {
					Anew[j][i] = 0.25 * ( A[j][i+1] + A[j][i-1]
										+ A[j-1][i] + A[j+1][i]);
					error = fmax( error, fabs(Anew[j][i] - A[j][i]));
				}
			}
			
			for( int j = 1; j < n-1; j++) {
				#pragma acc loop gang(8) vector(32)
				for( int i = 1; i < m-1; i++ ) {
					A[j][i] = Anew[j][i];  
				}
			}
		}

		// Print the iteration counter every 100 iters
		if(iter % 100 == 0) printf("%5d, %0.6f\n", iter, error);
		iter++;
	}

	// Get total runtime
	double runtime = GetTimer();
	printf(" total: %f s\n", runtime / 1000);

	return 0;
}
