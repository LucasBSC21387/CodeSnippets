# Description

The most difficult part of accelerator programming begins before the first line of code is written. If your program is not highly parallel, an accelerator or coprocesor won't be much use. Understanding the code structure is crucial if you are going to identify opportunities and successfully parallelize a piece of code. The first step in OpenACC programming then is to characterize the application. This includes:
- Understanding the program structure and how data is passed through the call tree
- Profiling the CPU-only version of the application and identifying computationally-intense "hot spots"
	- Which loop nests dominate the runtime?
	- Are the loop nests suitable for an accelerator?
- Ensuring that the algorithms you are considering for acceleration are safely parallel
This may sound a little scary, but please note that as parallel programming methods go, OpenACC is really friendly; because OpenACC directives are incremental, you can add one or two directives at a time and see how things work. Also, the compiler provides a lot of feedback that can help you during the acceleration process.

In this example, an accelerated 2D-stencil Jacobi iteration is provided. Jacobi iteration is a standard method for finding solutions to a system of linear equations.

### A note on performance

The compiler can do the hard work of mapping loop nests, unless you are certain you can do it better (and portability is not a concern.) When you decide to intervene, think about different parallelization strategies (loop schedules): in nested loops, distributing the work of the outer loops to the GPU multiprocessors (on PGI = gangs) in 1D grids. Similarly, think about mapping the work of the inner loops to the cores of the multiprocessors (CUDA threads, vectors) in 1D blocks. The grids (gangs) and block (vector) sizes can be viewed by setting the environment variable ACC_NOTIFY.

# Pre-requisites

- NVIDIA-HPC-SDK (any version); or
- any OpenACC aware compiler;

# Building

Using nvc:
```
nvc -fast -acc -Minfo -o [ExecName] jacobi_openacc.c
```
The "-Minfo" command-line option will get all the output messages the compiler can provide. Instead, to se only the output corresponding to the accelerator, use "-Minfo=accel".

# Usage

The executable should be run with:

```
./[ExecName]
```

To collect some timing information regarding the execution, the following environmental variable can be set before running the compiled code:

```
export PGI_ACC_TIME=1
```
