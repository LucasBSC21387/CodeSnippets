# CodeSnippets

Space to deposit interesting pieces of code and discuss programming issues/challenges/novelties. The idea is to create a "code knowledge" library to share between members.

## Getting started

For now, just deposit a folder containing the source code and either a Makefile or a CMake configuration or some text
file containing build instructions and possiblle dependencies. Later, we can propose some form of organization.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)

***
