/*
	VECTOR ADDITION IN CUDA

	Simple vector addition to help understand the structure of a CUDA program. 
	The goals of this snippet are:
		- How to allocate memory in the GPU
		- How to copy data between host and GPU memories
		- How to use the thread and block indexes to write CUDA kernels
		- How to invoke GPU kernels
*/

#include <stdio.h>
#include <stdlib.h>

// Helpful macros
#define ERROR(msg) \
	fprintf(stderr, "[%s:%d] %s \n", __FILE__, __LINE__,msg);\
	fflush(stderr);\
	exit(-1);\

#define div_and_ceil(a,b) ceil(((a) + ((b)-1))/(b))

#define NTHREADS 512

// CUDA Kernel
__global__ void vecAdd(float *C, const float * __restrict__ A, const float * __restrict__ B,
		const unsigned sz) {
/*
	NOTE: the __restrict__ attribute tells the compiler that the memory pointed to by the 
	pointer is not referenced by any other pointer argument. This may allow the compiler to 
	better optimize the code. However, is the programmers responsibility to make sure no other
	pointer argument points to the same memory, otherwise, the results may be wrong.
*/
	int idx = threadIdx.x + (blockIdx.x*blockDim.x);

	if (idx < sz) C[idx] = A[idx] + B[idx];
}

// Helper functions
void readVector(const char *fName, float **vec_h, unsigned *size)
{
	FILE* fp = fopen(fName, "rb");
	if (fp == NULL) { ERROR("Cannot open input file"); }

	fread(size, sizeof(unsigned), 1, fp);
	*vec_h = (float*)malloc(*size * sizeof(float));
	if(*vec_h == NULL) { ERROR("Unable to allocate host"); }
	fread(*vec_h, sizeof(float), *size, fp);
	fclose(fp);
}

void writeVector(const char *fName, float *vec_h, unsigned size)
{
    FILE* fp = fopen(fName, "wb");
    if (fp == NULL) { ERROR("Cannot open output file"); }
    fwrite(&size, sizeof(unsigned), 1, fp);
    fwrite(vec_h, sizeof(float), size, fp);
    fclose(fp);
}


// Main function
int main(int argc, char *argv[])
{
	float *A_h, *B_h, *C_h;
	float *A_d, *B_d, *C_d;
	unsigned vec_size;
	cudaError_t cuda_ret;

	const char *inputVec1 = "input1.dat";
	const char *inputVec2 = "input2.dat";
	const char *outputVec = "output.dat";

	/* Initialize input vectors */
	readVector(inputVec1, &A_h, &vec_size);
	readVector(inputVec2, &B_h, &vec_size);

	/* Allocate host memory */
	C_h = (float *)malloc(vec_size*sizeof(float));
	if(C_h == NULL) { ERROR("Unable to allocate host"); }

	/********************************************************************
	Allocate device memory for the input/output vectors
	********************************************************************/
	cuda_ret = cudaMalloc((void**)&A_d,vec_size*sizeof(float));
	if(cuda_ret != cudaSuccess) { ERROR("Unable to set device memory"); }
	cuda_ret = cudaMalloc((void**)&B_d,vec_size*sizeof(float));
	if(cuda_ret != cudaSuccess) { ERROR("Unable to set device memory"); }
	cuda_ret = cudaMalloc((void**)&C_d,vec_size*sizeof(float));
	if(cuda_ret != cudaSuccess) { ERROR("Unable to set device memory"); }

	/********************************************************************
	Copy the input vectors from the host memory to the device memory
	********************************************************************/
	cuda_ret = cudaMemcpy(A_d, A_h, vec_size*sizeof(float), cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) { ERROR("Unable to copy to device memory"); }
	cuda_ret = cudaMemcpy(B_d, B_h, vec_size*sizeof(float), cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) { ERROR("Unable to copy to device memory"); }
	cuda_ret = cudaMemcpy(C_d, C_h, vec_size*sizeof(float), cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) { ERROR("Unable to copy to device memory"); }

	cuda_ret = cudaMemset(C_d, 0, vec_size * sizeof(float));
	if(cuda_ret != cudaSuccess) { ERROR("Unable to copy to device memory"); }

	/********************************************************************
	Initialize thread block and kernel grid dimensions
	********************************************************************/
	dim3 dimGrid(div_and_ceil(vec_size,NTHREADS),1,1);
	dim3 dimBlock(NTHREADS,1,1);

	/********************************************************************
	Invoke CUDA kernel
	********************************************************************/
	vecAdd<<<dimGrid,dimBlock>>>(C_d, A_d, B_d, vec_size);

	cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) { ERROR("Unable to launch/execute kernel"); }

	/********************************************************************
	Copy the result back to the host
	********************************************************************/
	cuda_ret = cudaMemcpy(C_h, C_d, vec_size*sizeof(float), cudaMemcpyDeviceToHost);
	if(cuda_ret != cudaSuccess) { ERROR("Unable to copy from device memory"); }

	writeVector(outputVec, C_h, vec_size);
	/********************************************************************
	Free device memory allocations
	********************************************************************/
	cudaFree(A_d); cudaFree(B_d); cudaFree(C_d);
	free(A_h);     free(B_h);     free(C_h);

	return 0;
}
