# Description

Simple vector addition to help understand the structure of a CUDA program. The goals of this snippet are:
- How to allocate memory in the GPU
- How to copy data between host and GPU memories
- How to use the thread and block indexes to write CUDA kernels
- How to invoke GPU kernels

# Pre-requisites

- CUDA libraries (any version);

# Building

Simply with the standard nvcc:
```
nvcc -o [ExecName] vecAdd_cuda.cu
```

# Usage

The executable should be run with:

```
./[ExecName]
```
