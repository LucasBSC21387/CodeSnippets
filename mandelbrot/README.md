# Description

Small example of a Mandelbrot set using different parallel approaches:
- classical OpenMP.
- MPI with domain decomposition.
- CUDA.
- OpenACC, both with managed memory and with explicit memory allocation.

# Pre-requisites

- OpenMP aware compiler;
- MPI compiler;
- NVIDIA-HPC-SDK (any version); or
- any OpenACC aware compiler;
- CUDA libraries (any version);

# Building

#### OpenMP example

Using gcc:
```
gcc -Ofast -fopenmp -o [ExecName] mandelbrot_omp.c
```
or
```
icc -Ofast -qopenmp -o [ExecName] mandelbrot_omp.c
```

#### MPI example

Using gcc:
```
mpicc -Ofast -o [ExecName] mandelbrot_mpi.c
```
or
```
mpiicc -Ofast -o [ExecName] mandelbrot_mpi.c
```

#### CUDA example

Simply with the standard nvcc:
```
nvcc -O3 -o [ExecName] mandelbrot_cuda.cu
```

#### OpenACC example

Using nvc:
```
nvc -fast -acc -Minfo -ta=tesla:managed -o [ExecName] mandelbrot_acc_managed.c
```
or
```
nvc -fast -acc -Minfo -ta=tesla -o [ExecName] mandelbrot_acc_explicit.c
```
The "-ta=tesla:managed" command-line option will use CUDA Unified Memory to eliminate the need for data management directives. Replace this compiler flag in the Makefile with "-ta=tesla" to specifically allocate the memory on the GPU.

The "-Minfo" command-line option will get all the output messages the compiler can provide. Instead, to se only the output corresponding to the accelerator, use "-Minfo=accel".

# Usage

The executable should be run with:

```
./[ExecName]
```

The MPI version should be run with:

```
mpirun -np [Nprocs] ./[ExecName] [Npx] [Npy]
```

where [Nprocs] are the total number of processors and [Npx] and [Npy] are the number of processors per direction. The condition to be followed is [Nprocs] = [Npx]\*[Npy].

On OpenACC, to collect some timing information regarding the execution, the following environmental variable can be set before running the compiled code:

```
export PGI_ACC_TIME=1
```
