/*
	MANDELBROT

	Small example of a Mandelbrot set using MPI, with
	domain partition.
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "timer.h"

#define WIDTH     8192
#define HEIGHT    8192
#define MAX_ITERS 100
#define MAX_COLOR 255

#define AC_IMG(i,j)   image[(i) + n_x*(j)]
#define AC_IMG_G(i,j) image_g[(i) + WIDTH*(j)]

#define xmin -1.7
#define xmax .5
#define ymin -1.2
#define ymax 1.2
#define dx   (xmax-xmin)/WIDTH
#define dy   (ymax-ymin)/HEIGHT


unsigned char mandelbrot(int Px, int Py) {
	
	double x = 0.0, x0 = xmin + Px*dx;
	double y = 0.0, y0 = ymin + Py*dy;
	int ii;

	for (ii=0; x*x+y*y<4.0 && ii < MAX_ITERS; ++ii) {
		double xtemp = x*x - y*y + x0;
		y = 2*x*y + y0;
		x = xtemp;
	}
	
	return (double)(MAX_COLOR*ii/MAX_ITERS);
}

void worksplit(int *mystart, int *myend, int rank, int size, int start, int end ) {

	int N = end - start;
	int NperProcess, remainder;

	if (size < N) {
		// We split normally among processes assuming no remainder
		NperProcess = N/size;
		*mystart = start    + rank*NperProcess;
		*myend   = *mystart + NperProcess;
		// Handle the remainder
		remainder = N - NperProcess*size;
		if (remainder > rank) {
			*mystart += rank;
			*myend   += rank+1;
		} else {
			*mystart += remainder;
			*myend   += remainder;
		}
	} else {
		// Each process will forcefully conduct one instant.
		*mystart = (rank < end) ? rank   : end;
		*myend   = (rank < end) ? rank+1 : end;
	}
}

void checkr(int r,char *txt) {
	if (r!=MPI_SUCCESS) {
		fprintf(stderr,"Error: %s\n",txt);
		exit(-1);
	}
}


int main(int argc, char **argv) {

	int npx, npy; // Number of processors in each direction
	int rank, rank_x, rank_y, size;
	int start_x, start_y, end_x, end_y, n_x, n_y;
	double runtime, runtime_g;

	size_t bytes, bytes_g; 
	unsigned char *image, *image_g; // Byte character, from 0 to 255

	// Initialize MPI communications
	checkr(MPI_Init(&argc,&argv),"init");

	// Recover rank and size
	checkr(MPI_Comm_rank(MPI_COMM_WORLD,&rank),"rank");
	checkr(MPI_Comm_size(MPI_COMM_WORLD,&size),"size");

	// Recover npx and npy from user input
	npx = atoi(argv[1]);
	npy = atoi(argv[2]);

	// Check limits
	if (npx*npy > size) {
		fprintf(stderr,"Error, invalid number of processors %d (%d)!\n",npx*npy,size);
		exit(-1);
	}

	// We need to map the global processor ID to the (x,y)
	// grid that we are creating
	rank_x = rank%npx;
	rank_y = rank/npx;
//	printf("Grid mapping of rank %d to (%d,%d)\n",rank,rank_x,rank_y);


	// Now that we have the processor gird specified
	// we can worksplit every dimension
	worksplit(&start_x,&end_x,rank_x,npx,0,WIDTH);  n_x = end_x - start_x;
	worksplit(&start_y,&end_y,rank_y,npy,0,HEIGHT); n_y = end_y	- start_y;
//	printf("Rank %d (%d,%d), start (%d,%d) end (%d,%d) n (%d,%d)\n",rank,rank_x,rank_y,start_x,start_y,end_x,end_y,n_x,n_y);

	// Allocate local image
	bytes = n_x*n_y*sizeof(unsigned char);
	image = (unsigned char*)malloc(bytes);
	if (image == NULL) {
		fprintf(stderr,"Cannot allocate memory!\n");
		exit(-1);
	}

	// Start the timer
	StartTimer();

	// Loop and update image
	// Loop on global x,y and store on local image
	for (int y=start_y; y<end_y; ++y)
		for(int x=start_x; x<end_x; ++x)
			AC_IMG(x-start_x,y-start_y) = mandelbrot(x,y);

	// Store a subimage per each processor
//	char aux[256];
//	sprintf(aux,"image_%d.pgm",rank);
//	FILE *fp=fopen(aux,"wb");
//	fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",n_x,n_y,MAX_COLOR);
//	fwrite(image,sizeof(unsigned char),n_x*n_y,fp);
//	fclose(fp);


	// Collage the subimages into the global image
	// a simple gather will not cut it since we need to relate the
	// local coordinates with the global coordinates

	// Store local image into global image
	if (rank == 0) {
		// Allocate global image
		bytes   = WIDTH*HEIGHT*sizeof(unsigned char);
		image_g = (unsigned char*)malloc(bytes);
		if (image_g == NULL) {
			fprintf(stderr,"Cannot allocate memory!\n");
			exit(-1);
		}
		// Collage image into global image
		for (int y=start_y; y<end_y; ++y)
			for(int x=start_x; x<end_x; ++x)
				AC_IMG_G(x,y) = AC_IMG(x-start_x,y-start_y);
	}

	for (int irank=1; irank<size; ++irank) {
		// irank sends image to rank 0
		if (rank == irank)
			checkr(MPI_Ssend(image,n_x*n_y,MPI_UNSIGNED_CHAR,0,11,MPI_COMM_WORLD),"send");
		// rank 0 receives image from irank and
		// stores into the global image
		if (rank == 0) {
			// Recompute the irank tile - reuse some variables
			rank_x = irank%npx; rank_y = irank/npx;
			worksplit(&start_x,&end_x,rank_x,npx,0,WIDTH);  n_x = end_x - start_x;
			worksplit(&start_y,&end_y,rank_y,npy,0,HEIGHT); n_y = end_y	- start_y;
			// Allocate image
			bytes = n_x*n_y*sizeof(unsigned char);
			image = (unsigned char*)realloc(image,bytes);
			// Receive image from irank
			checkr(MPI_Recv(image,n_x*n_y,MPI_UNSIGNED_CHAR,irank,11,MPI_COMM_WORLD,MPI_STATUS_IGNORE),"recieve");
			// Collage image into the global image
			for (int y=start_y; y<end_y; ++y)
				for(int x=start_x; x<end_x; ++x)
					AC_IMG_G(x,y) = AC_IMG(x-start_x,y-start_y);
		}
	}

	// Get total runtime
	runtime = GetTimer();
	checkr(MPI_Reduce(&runtime,&runtime_g,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD),"reduce");
	if (rank==0) printf(" total: %f s\n", runtime_g / size / 1000);

	// rank 0 stores the image
	if (rank == 0) {
		// Store as image
		FILE *fp=fopen("image.pgm","wb");
		fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",WIDTH,HEIGHT,MAX_COLOR);
		fwrite(image_g,sizeof(unsigned char),WIDTH*HEIGHT,fp);
		fclose(fp);
		// Free memory
		free(image_g);
	}

	free(image);
	MPI_Finalize();

	return 0;
}
