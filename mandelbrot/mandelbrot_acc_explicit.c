/*
	MANDELBROT

	Small example of a Mandelbrot set using OpenACC, both with managed memory 
	and with explicit memory allocation.

	Explicit memory version.
*/

#include <stdio.h>
#include <stdlib.h>

#include "timer.h"

#define WIDTH     8192
#define HEIGHT    8192
#define MAX_ITERS 100
#define MAX_COLOR 255

#define AC_IMG(i,j) image[(i) + WIDTH*(j)]

#define xmin -1.7
#define xmax .5
#define ymin -1.2
#define ymax 1.2
#define dx   (xmax-xmin)/WIDTH
#define dy   (ymax-ymin)/HEIGHT


unsigned char mandelbrot(int Px, int Py) {
	
	double x = 0.0, x0 = xmin + Px*dx;
	double y = 0.0, y0 = ymin + Py*dy;
	int ii;

	for (ii=0; x*x+y*y<4.0 && ii < MAX_ITERS; ++ii) {
		double xtemp = x*x - y*y + x0;
		y = 2*x*y + y0;
		x = xtemp;
	}
	
	return (double)(MAX_COLOR*ii/MAX_ITERS);
}


int main(int argc, char const *argv[]) {

	// Byte character, from 0 to 255
	size_t bytes = WIDTH*HEIGHT*sizeof(unsigned char);
	unsigned char *image=(unsigned char*)malloc(bytes);
	
	StartTimer();

	// Loop and update image
	#pragma acc parallel loop collapse(2) copy(image[:bytes]) device_type(nvidia) vector_length(32)
	for (int y=0; y<HEIGHT; ++y)
		for(int x=0; x<WIDTH; ++x)
			AC_IMG(x,y)=mandelbrot(x,y);

	// Get total runtime
	double runtime = GetTimer();
	printf(" total: %f s\n", runtime / 1000);

	// Store as image
	FILE *fp=fopen("image.pgm","wb");
	fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",WIDTH,HEIGHT,MAX_COLOR);
	fwrite(image,sizeof(unsigned char),WIDTH*HEIGHT,fp);
	fclose(fp);

	free(image);
	return 0;
}
