/*
	MANDELBROT

	Small example of a Mandelbrot set using CUDA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "timer.h"

#define WIDTH     8192
#define HEIGHT    8192
#define TILE_SIZE 16
#define MAX_ITERS 100
#define MAX_COLOR 255

#define AC_IMG(i,j)       image[(i) + WIDTH*(j)]
#define div_and_ceil(a,b) ceil(((a) + ((b)-1))/(b))

#define xmin -1.7
#define xmax .5
#define ymin -1.2
#define ymax 1.2
#define dx   (xmax-xmin)/WIDTH
#define dy   (ymax-ymin)/HEIGHT


__device__ unsigned char mandelbrot(int Px, int Py) {
	
	double x = 0.0, x0 = xmin + Px*dx;
	double y = 0.0, y0 = ymin + Py*dy;
	int ii;

	for (ii=0; x*x+y*y<4.0 && ii < MAX_ITERS; ++ii) {
		double xtemp = x*x - y*y + x0;
		y = 2*x*y + y0;
		x = xtemp;
	}
	
	return (double)(MAX_COLOR*ii/MAX_ITERS);
}

__global__ void mandelbrot_kernel(unsigned char *image) {
	
	unsigned int x = threadIdx.x + (blockIdx.x*blockDim.x);
	unsigned int y = threadIdx.y + (blockIdx.y*blockDim.y);

	if (x<WIDTH && y<HEIGHT)
		AC_IMG(x,y) = mandelbrot(x,y);
}


int main(int argc, char const *argv[]) {

	// Byte character, from 0 to 255
	size_t bytes = WIDTH*HEIGHT*sizeof(unsigned char);
	unsigned char *image, *image_d;

	// Define blocks and threads
	dim3 dimGrid(div_and_ceil(WIDTH,TILE_SIZE),div_and_ceil(HEIGHT,TILE_SIZE),1);
	dim3 dimBlock(TILE_SIZE,TILE_SIZE,1);

	// Allocate memory on host and the GPU
	cudaHostAlloc((void**)&image,bytes,cudaHostAllocDefault);
	
	StartTimer();
	cudaMalloc((void**)&image_d,bytes);

	// Launch CUDA kernel
	mandelbrot_kernel<<<dimGrid,dimBlock>>>(image_d);

	// Copy image from device to host
	cudaMemcpy(image,image_d,bytes,cudaMemcpyDeviceToHost);
	cudaFree(image_d);

	// Get total runtime
	double runtime = GetTimer();
	printf(" total: %f s\n", runtime / 1000);

	// Store as image
	FILE *fp=fopen("image.pgm","wb");
	fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",WIDTH,HEIGHT,MAX_COLOR);
	fwrite(image,sizeof(unsigned char),WIDTH*HEIGHT,fp);
	fclose(fp);

	cudaFreeHost(image);
	return 0;
}
