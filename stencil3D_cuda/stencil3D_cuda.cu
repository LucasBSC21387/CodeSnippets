/*
STENCIL 3D IN CUDA

	Tiling is a common technique used to improve data locality and minimize accesses 
	to main memory. This code snippet extends a naive implementation of the 7-point 
	3D stencil kernel to use tiling to reduce accesses to global memory. 

	The goals are:
		- How to use shared memory to store values that are read by many threads within 
		a thread block. (Shared memory tiling)
		- How to use registers to store values that are used many times by the same thread. 
		(Register tiling)
		- Why the size of thread blocks is important when using shared memory.
*/

#include <stdio.h>
#include <stdlib.h>

// Helpful macros
#define CHECK_CUDA_ERROR() do { \
	cudaError_t err = cudaGetLastError(); \
	if( cudaSuccess != err) { \
	fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
	__FILE__, __LINE__, cudaGetErrorString( err) ); \
	exit(EXIT_FAILURE); \
	} \
	} while(0)

// Returns x/y ceiled to the next (upper) integer when x doesn't evenly 
// divide by y (uses integer arithmetic trickery)
#define div_and_ceil(x,y) (((x) - 1)/(y) + 1)

// Macros for accessing flattened matrices
#define Index3D(_i,_j,_k) ( (_i) + nx * ((_j) + ny*(_k)) )

// Parameters of tile sizes (only needed for the tiled implementation)
#define TILE_SIZE 16

// CUDA Kernel implementations
#ifdef STENCIL_SIMPLE

__global__ void stencil_simple(float fac, const float *in, float *out, int nx, int ny, int nz) {
	
	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;

	if( i>0 && j>0 &&(i<nx-1) &&(j<ny-1) ){ //ignore the border cells
		for(int k=1;k<nz-1;k++){
			out[Index3D (i, j, k)] =
			in[Index3D (i, j, k + 1)] +
			in[Index3D (i, j, k - 1)] +
			in[Index3D (i, j + 1, k)] +
			in[Index3D (i, j - 1, k)] +
			in[Index3D (i + 1, j, k)] +
			in[Index3D (i - 1, j, k)]
			- 6.0f * in[Index3D (i, j, k)] / (fac*fac);
		}
	}
}

#else

__global__ void stencil_block2D(float fac, const float *in, float *out, int nx, int ny, int nz) {

	// Aliases for readability
	int tx = threadIdx.x, ty = threadIdx.y;
	int bx = blockIdx.x,  by = blockIdx.y;
	int dx = blockDim.x,  dy = blockDim.y;

	// Global x and y indexes of the thread
	int i  = bx*dx + tx;
	int j  = by*dy + ty;
	int k  = 1;

	// Shared memory to reuse from all the threads
	// Added +2 on every dimensions to deal with the halos
	__shared__ float sh_in[TILE_SIZE+2][TILE_SIZE+2]; 

	// Registries for back, front and current
	float back    = in[Index3D(i,j,k-1)];
	float current = in[Index3D(i,j,k)];
	float front;

	//Iterate over xy planes in the z dimension
	for (k=1;k<nz-1;k++) {
		//Load the element from the xy plane on shared memory
		sh_in[tx+1][ty+1] = current;
		// Fill in the halos
		if (tx == 0)    sh_in[0][ty+1]    = (i>0)    ? in[Index3D(i-1,j,k)] : 0.;
		if (tx == dx-1) sh_in[dx+1][ty+1] = (i<nx-1) ? in[Index3D(i+1,j,k)] : 0.;
		if (ty == 0)    sh_in[tx+1][0]    = (j>0)    ? in[Index3D(i,j-1,k)] : 0.;
		if (ty == dy-1) sh_in[tx+1][dy+1] = (j<ny-1) ? in[Index3D(i,j+1,k)] : 0.;
		__syncthreads();

		// Get values from shared memory
		float left   = sh_in[tx+1][ty+2];
		float right  = sh_in[tx+1][ty];
		float bottom = sh_in[tx][ty+1];
		float top    = sh_in[tx+2][ty+1];
		//Check global boundaries
		if(i>0 && j>0 && (i<nx-1) &&(j<ny-1)) {
			left   = in[Index3D(i-1,j,k)];
			right  = in[Index3D(i+1,j,k)];
			bottom = in[Index3D(i,j-1,k)];
			top    = in[Index3D(i,j+1,k)];
			//Load the front
			front = in[Index3D(i,j,k+1)];

			// Compute the stencil
			out[Index3D(i,j,k)] = back + front + left + right + top + bottom - 6.0f * current / (fac * fac);
		}
		__syncthreads();

		// Update registry values
		back    = current;
		current = front;
	}
}

#endif

// Helper functions
void execute_kernel(float fac, const float* d_in, float* d_out, int nx, int ny, int nz)  {

	// Ensure to have nz threads per block
	const int grid_x = div_and_ceil(nx,TILE_SIZE);
	const int grid_y = div_and_ceil(ny,TILE_SIZE);

	// Define grid and block sizes
	dim3 block (TILE_SIZE,TILE_SIZE,1); // Block size should correspond to the tile size (given in the code)
	dim3 grid  (grid_x,grid_y,1);

#ifdef STENCIL_SIMPLE
	printf("Using the SIMPLE kernel implementation\n");
	stencil_simple<<<grid, block>>>(fac, d_in, d_out, nx, ny, nz);
#else
	printf("Using the TILED kernel implementation\n");
	stencil_block2D<<<grid, block>>>(fac, d_in, d_out, nx, ny, nz);
#endif

	CHECK_CUDA_ERROR();
}

int generate_data(float *A0, int nx,int ny,int nz) {	
	srand(54321); int s=0;
	for(int i=0;i<nz;i++) {
		for(int j=0;j<ny;j++) {
			for(int k=0;k<nx;k++) {
				A0[s++] = (rand() / (float) RAND_MAX);
			}
		}
	}
	return 0;
}

// Main function
int main(int argc, char** argv) {

	const int nx = 512, ny = 512, nz = 64;
	const int size = nx*ny*nz;
	printf ("SIZE = %dx%dx%d\n", nx, ny, nz);
	
	// Host data
	float *h_A0, *h_Anext;
	
	h_A0    = (float*)malloc(sizeof(float)*size);
	h_Anext = (float*)malloc(sizeof(float)*size);

	generate_data(h_A0, nx,ny,nz);
	float fac = h_A0[0];

	// Device data
	float *d_A0, *d_Anext;


	// Device memory allocation
	cudaMalloc((void **)&d_A0,    size*sizeof(float)); CHECK_CUDA_ERROR();
	cudaMalloc((void **)&d_Anext, size*sizeof(float)); CHECK_CUDA_ERROR();
	
	cudaMemset(d_Anext,0,size*sizeof(float)); CHECK_CUDA_ERROR();

	// Memory copy
	cudaMemcpy(d_A0, h_A0, size*sizeof(float), cudaMemcpyHostToDevice); CHECK_CUDA_ERROR();
	
	// Main execution
	execute_kernel(fac, d_A0, d_Anext, nx, ny, nz);
	
	cudaDeviceSynchronize();
    CHECK_CUDA_ERROR();

    // Recover from device
	cudaMemcpy(h_Anext, d_Anext,size*sizeof(float), cudaMemcpyDeviceToHost);
	CHECK_CUDA_ERROR();

	// Free memory
    cudaFree(d_A0); CHECK_CUDA_ERROR();
    cudaFree(d_Anext); CHECK_CUDA_ERROR();
 	
	free (h_A0); free (h_Anext);

	return 0;
}