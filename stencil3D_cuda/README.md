# Description

Tiling is a common technique used to improve data locality and minimize accesses to main memory. This code snippet extends a naive implementation of the 7-point 3D stencil kernel to use tiling to reduce accesses to global memory. The goals are:
- How to use shared memory to store values that are read by many threads within a thread block. (Shared memory tiling)
- How to use registers to store values that are used many times by the same thread. (Register tiling)
- Why the size of thread blocks is important when using shared memory.

# Background

Stencil computations are widely used in scientific simulations. In a k-order stencil computation, the value of each cell of the output array is computed as a linear combination of the central value and the values of the k neighbouring cells of the input volume in each dimension. The figure below shows the 7 values of the 3D input array used to compute the output value for the (i, j, k) cell in a 1-order stencil computation (also called 7-point stencil).

<img src="https://gitlab.com/LucasBSC21387/CodeSnippets/-/raw/main/stencil3D_cuda/stencil.png"/>

This code you iterates through the z dimension of 3D volume, applying the stencil above to the elements of one plane (X-Y) at a time. The naive implementation is provided in the code as the simple2D_stencil. An optimized version uses shared memory tiling and register tiling techniques. The idea behind these techniques is to temporarily store a part of the input set that is reused for several operations in a memory local to the SM (either registers or shared memory). Since these local memories have shorter access times, subsequent loads of the data are faster, improving the overall kernel performance.

# Pre-requisites

- CUDA libraries (any version);

# Building

Simply with the standard nvcc:
```
nvcc -o [ExecName] stencil3D_cuda.cu
```

To obtain the simple scheme use:
```
nvcc -DSTENCIL_SIMPLE -o [ExecName] stencil3D_cuda.cu
```

# Usage

The executable should be run with:

```
./[ExecName]
```
